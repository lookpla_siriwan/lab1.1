/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

import java.sql.ResultSet;

/**
 *
 * @author Room107
 */
public interface DBConnection {
    ResultSet executeQuery(String query);
    void update(String command);
    void close();
}
